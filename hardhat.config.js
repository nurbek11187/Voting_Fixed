require("dotenv").config();

require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-waffle");
require("hardhat-gas-reporter");
require("solidity-coverage");
Web3 = require('web3')
const jsonabi = require('./artifacts/contracts/Voting.sol/Voting.json')
const Tx = require('ethereumjs-tx').Transaction
const { task } = require("hardhat/config");






// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const Contract = require("web3-eth-contract");

var web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/e3cee0bae01f47a8a18e4debf2cd0063'));
// создание контракта на web3.js 

var contract = new web3.eth.Contract(jsonabi.abi, "0xc4061ab8e28ea0e5450E91f754A0178833412583");



 task('create_vote', "test desctiption")
 .setAction(async () => {
   contractMethod = contract.methods.createVoting(
     ["0x4c54a50B8F9D4f2EBfa187f4633A581440096678", "0xb62031Fc947998cB9B55f170566D521E7EdEEF40"],
     ["123", "123"]
   )
   account = web3.utils.toChecksumAddress("0x479268d467648bb567db1b8fd3b7549498db71fc");
   const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


   const txObject = await web3.eth.accounts.signTransaction ({
     nonce: transactionCount,
     gas: '800000',
     to: '0xc4061ab8e28ea0e5450E91f754A0178833412583',
     gasPrice: web3.utils.toWei('10', 'gwei'),
     data: contractMethod.encodeABI()
   },
   `${process.env.PRIVATE_KEY}`
   )
   // Sign the transaction
   const createReceipt = await web3.eth.sendSignedTransaction(
     txObject.rawTransaction
  );
  console.log(
     `Transaction successful with hash: ${createReceipt.transactionHash}`
  );
})

task('vote', "vote for candidate")
  .setAction(async () => {

    contractMethod = contract.methods.vote(0,0)
    account = '0x479268d467648bb567db1b8fd3b7549498db71fc';
    const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


    const txObject = await web3.eth.accounts.signTransaction ({
      nonce: transactionCount,
      gas: '800000',
      to: '0xc4061ab8e28ea0e5450E91f754A0178833412583',
      value: web3.utils.toWei('0.01','ether'),
      gasPrice: web3.utils.toWei('10', 'gwei'),
      data: contractMethod.encodeABI()
    },
    `${process.env.PRIVATE_KEY}`
    )
    const createReceipt = await web3.eth.sendSignedTransaction(
      txObject.rawTransaction
   );
   console.log(
      `Transaction successful with hash: ${createReceipt.transactionHash}`
   );
})

//Этот таск работает, но на контракте выйдет ошибка что (Fail with error 'Not time to end vote') если подождете 3 дня то можно закрыть
task('endvote', "end voting")
  .setAction(async () => {
    contractMethod = contract.methods.endVote(0)
    account = '0x479268d467648bb567db1b8fd3b7549498db71fc';
    const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


    const txObject = await web3.eth.accounts.signTransaction ({
      nonce: transactionCount,
      gas: '800000',
      to: '0xc4061ab8e28ea0e5450E91f754A0178833412583',
      gasPrice: web3.utils.toWei('10', 'gwei'),
      data: contractMethod.encodeABI()
    },
    `${process.env.PRIVATE_KEY}`
    )
    const createReceipt = await web3.eth.sendSignedTransaction(
      txObject.rawTransaction
   );
   console.log(
      `Transaction successful with hash: ${createReceipt.transactionHash}`
   );
})



module.exports = {
  solidity: "0.8.4",
  networks: {
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${process.env.ALCHEMY_API_KEY}`,
      accounts: [`${process.env.PRIVATE_KEY}`]
    }
  },
  hardhat:{
    chainId: 1337
  },
  etherscan: {
    apiKey: `${process.env.ETHERSCAN_KEY}`
  }

};
