const { expect } = require("chai")
const { ethers } = require("hardhat")


describe("Voting", function(){
    let acc1
    let acc2
    let acc3
    let acc4
    let acc5
    let voting
    beforeEach(async function(){
        [acc1, acc2, acc3, acc4, acc5] = await ethers.getSigners()
        const Voting = await ethers.getContractFactory("Voting", acc1)
        voting = await Voting.deploy()
        await voting.deployed()
        console.log(voting.address)
    })
    it("should be deployed", async function(){
        expect(voting.address).to.be.properAddress
    })
    it("start voting", async function(){
        const startTransaction = await voting.createVoting([acc4.address, acc3.address, acc5.address], ["txt", "txt1", "txt2"])
        await startTransaction.wait()
       
    })


    it("list of candidates", async function(){
        const listOfCandidates = await voting.getVotingInformation(1)
    })

    it('get voters array', async function(){
        const getVotersArray = await voting.getVotersArray(0)
    })
    it('get voting information', async function(){
        const getVotersArray = await voting.getVotingInformation(0)
    })

    it("vote for candidate", async function(){
        let value1 = ethers.utils.parseEther("0.01")
        const voteToCandidate = await voting.vote(0,0, {value:value1})
    })
})  